import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("@/views/home/index.vue"),
    redirect: { name: "pageList" },
    children: [
      {
        path: "page-list",
        name: "pageList",
        component: () => import("@/views/home/page-list.vue")
      }
    ]
  },
  {
    path: "/fixed",
    name: "fixed",
    component: () => import("@/views/fixed/index.vue")
  },
  {
    path: "/long",
    name: "long",
    component: () => import("@/views/long/index.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
